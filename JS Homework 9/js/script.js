/*

## Задание

Реализовать переключение вкладок (табы) на чистом Javascript.

#### Технические требования:
- В папке `tabs` лежит разметка для вкладок. 
    Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. 
    При этом остальной текст должен быть скрыт. 
    В комментариях указано, какой текст должен отображаться для какой вкладки. 
- Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
- Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. 
    При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

#### Литература:
- [HTMLElement.dataset](https://developer.mozilla.org/ru/docs/Web/API/HTMLElement/dataset)

*/

const tabsBtn = document.querySelectorAll(".tabs__nav-btn");
const tabsItems = document.querySelectorAll(".tabs__item");


tabsBtn.forEach(onTabClick);

function onTabClick(item) {
    item.addEventListener('click', function () {
        let currentBtn = item;
        let tabId = currentBtn.getAttribute('data-tab');
        let currentTab = document.querySelector(tabId);

        if (!currentBtn.classList.contains('active')) {
            tabsBtn.forEach(function (item) {
                item.classList.remove('active');
            });
    
            tabsItems.forEach(function (item) {
                item.classList.remove('active');
            });
    
            currentBtn.classList.add('active');
            currentTab.classList.add('active');
        }
    });
};

document.querySelector('.tabs__nav-btn').click();